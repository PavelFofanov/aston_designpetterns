package com.PavelFofanov.Generating_Patterns.FactoryMethod;

public class TransportFactory {

    public Transport createTransport(int power){
        if (power < 500){
            return new Car();
        }else
            return new Truck();
    }
}
