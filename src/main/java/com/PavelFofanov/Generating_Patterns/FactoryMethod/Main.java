package com.PavelFofanov.Generating_Patterns.FactoryMethod;

public class Main {
    public static void main(String[] args) {
        Transport transport1 = new TransportFactory().createTransport(1000);
        Transport transport2 = new TransportFactory().createTransport(105);
        System.out.println(transport1.getType());
        System.out.println(transport2.getType());

    }
}
