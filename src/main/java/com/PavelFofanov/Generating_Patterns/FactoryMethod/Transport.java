package com.PavelFofanov.Generating_Patterns.FactoryMethod;

public interface Transport {
    String getType();
}
