package com.PavelFofanov.Generating_Patterns.FactoryMethod;

public class Truck implements Transport{
    @Override
    public String getType() {
        return "it's a Truck!";
    }
}
