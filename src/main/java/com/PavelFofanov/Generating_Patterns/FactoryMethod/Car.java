package com.PavelFofanov.Generating_Patterns.FactoryMethod;

public class Car implements Transport{
    @Override
    public String getType() {
        return "it's a Car";
    }
}
