package com.PavelFofanov.Generating_Patterns.Prototype;

public class CarFactory {
    Car car;

    public CarFactory(Car car) {
        this.car = car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    Car carCopy(){
        return (Car) car.copy();
    }
}
