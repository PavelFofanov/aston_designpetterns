package com.PavelFofanov.Generating_Patterns.Prototype;

public class Main {
    public static void main(String[] args) {
        Car car = new Car("Lada-Sedan", "Baklazhan!");

        CarFactory carFactory = new CarFactory(car);
        Car carClone = carFactory.carCopy();

        System.out.println(car);
        System.out.println("\n==================\n");
        System.out.println(carClone);
    }
}
