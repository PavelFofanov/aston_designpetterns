package com.PavelFofanov.Generating_Patterns.Prototype;

public interface Copyable {
    Object copy();
}
