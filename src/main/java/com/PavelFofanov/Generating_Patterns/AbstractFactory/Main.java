package com.PavelFofanov.Generating_Patterns.AbstractFactory;

import com.PavelFofanov.Generating_Patterns.AbstractFactory.iPhone.iPhoneSet;
import com.PavelFofanov.Generating_Patterns.AbstractFactory.nokia.NokiaSet;

public class Main {
    public static void main(String[] args) {
        ShoppingFactory shoppingFactoryNokia = new NokiaSet();
        ShoppingFactory shoppingFactoryiPhone = new iPhoneSet();
        System.out.println(byeSetForPhone(shoppingFactoryNokia));
        System.out.println(byeSetForPhone(shoppingFactoryiPhone));
        System.out.println(shoppingFactoryiPhone);


    }

    public static String byeSetForPhone(ShoppingFactory shoppingFactory){
        return shoppingFactory.getPhone().getName() + ", " +
                shoppingFactory.getCase().getColorCase() + ", " +
                shoppingFactory.getHeadphones().getType();
    }
}
