package com.PavelFofanov.Generating_Patterns.AbstractFactory;

public interface Phone {

    public String getName();
}
