package com.PavelFofanov.Generating_Patterns.AbstractFactory;

public interface Headphones {

    public String getType();
}
