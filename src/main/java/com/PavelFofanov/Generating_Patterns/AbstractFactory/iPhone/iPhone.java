package com.PavelFofanov.Generating_Patterns.AbstractFactory.iPhone;

import com.PavelFofanov.Generating_Patterns.AbstractFactory.Phone;

public class iPhone implements Phone {
    @Override
    public String getName() {
        return "Phone is iPhone";
    }
}
