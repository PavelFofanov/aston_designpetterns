package com.PavelFofanov.Generating_Patterns.AbstractFactory.iPhone;

import com.PavelFofanov.Generating_Patterns.AbstractFactory.Case;
import com.PavelFofanov.Generating_Patterns.AbstractFactory.Headphones;
import com.PavelFofanov.Generating_Patterns.AbstractFactory.Phone;
import com.PavelFofanov.Generating_Patterns.AbstractFactory.ShoppingFactory;

public class iPhoneSet implements ShoppingFactory {
    @Override
    public Phone getPhone() {
        return new iPhone();
    }

    @Override
    public Case getCase() {
        return new CaseForiPhone();
    }

    @Override
    public Headphones getHeadphones() {
        return new HeadphonesForiPhone();
    }

}
