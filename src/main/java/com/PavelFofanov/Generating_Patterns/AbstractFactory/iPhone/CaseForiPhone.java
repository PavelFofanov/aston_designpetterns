package com.PavelFofanov.Generating_Patterns.AbstractFactory.iPhone;

import com.PavelFofanov.Generating_Patterns.AbstractFactory.Case;

public class CaseForiPhone implements Case {
    @Override
    public String getColorCase() {
        return "Color of case is green";
    }
}
