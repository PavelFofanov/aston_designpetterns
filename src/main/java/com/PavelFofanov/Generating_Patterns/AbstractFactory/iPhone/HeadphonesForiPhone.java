package com.PavelFofanov.Generating_Patterns.AbstractFactory.iPhone;

import com.PavelFofanov.Generating_Patterns.AbstractFactory.Headphones;

public class HeadphonesForiPhone implements Headphones {
    @Override
    public String getType() {
        return "Type is wireless";
    }
}
