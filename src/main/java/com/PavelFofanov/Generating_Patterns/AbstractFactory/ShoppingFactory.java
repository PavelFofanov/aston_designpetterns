package com.PavelFofanov.Generating_Patterns.AbstractFactory;

public interface ShoppingFactory {

    Phone getPhone();

    Case getCase();

    Headphones getHeadphones();
}
