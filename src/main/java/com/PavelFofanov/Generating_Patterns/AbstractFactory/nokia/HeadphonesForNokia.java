package com.PavelFofanov.Generating_Patterns.AbstractFactory.nokia;

import com.PavelFofanov.Generating_Patterns.AbstractFactory.Headphones;

public class HeadphonesForNokia implements Headphones {
    @Override
    public String getType() {
        return "type is wired";
    }
}
