package com.PavelFofanov.Generating_Patterns.AbstractFactory.nokia;

import com.PavelFofanov.Generating_Patterns.AbstractFactory.Case;

public class CaseForNokia implements Case {
    @Override
    public String getColorCase() {
        return "Color case for Nokia is red";
    }
}
