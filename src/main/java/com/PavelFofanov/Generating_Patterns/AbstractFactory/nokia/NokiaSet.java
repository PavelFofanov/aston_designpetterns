package com.PavelFofanov.Generating_Patterns.AbstractFactory.nokia;

import com.PavelFofanov.Generating_Patterns.AbstractFactory.Case;
import com.PavelFofanov.Generating_Patterns.AbstractFactory.Headphones;
import com.PavelFofanov.Generating_Patterns.AbstractFactory.Phone;
import com.PavelFofanov.Generating_Patterns.AbstractFactory.ShoppingFactory;

public class NokiaSet implements ShoppingFactory {
    @Override
    public Phone getPhone() {
        return new Nokia();
    }

    @Override
    public Case getCase() {
        return new CaseForNokia();
    }

    @Override
    public Headphones getHeadphones() {
        return new HeadphonesForNokia();
    }
}
