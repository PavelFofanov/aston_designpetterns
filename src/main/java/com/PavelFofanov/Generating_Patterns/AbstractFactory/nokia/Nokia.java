package com.PavelFofanov.Generating_Patterns.AbstractFactory.nokia;

import com.PavelFofanov.Generating_Patterns.AbstractFactory.Phone;

public class Nokia implements Phone {
    @Override
    public String getName() {
        return "Phone Nokia";
    }
}
