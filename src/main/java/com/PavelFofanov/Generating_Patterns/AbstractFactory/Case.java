package com.PavelFofanov.Generating_Patterns.AbstractFactory;

public interface Case {

    public String getColorCase();

}
