package com.PavelFofanov.Generating_Patterns.Singleton;

public class Singleton {

    private static Singleton singleton;
    private Singleton(String value){}

    public static Singleton getInstance(String value){
        if (singleton == null){
            singleton = new Singleton(value);
        }
        return singleton;
    }


}
