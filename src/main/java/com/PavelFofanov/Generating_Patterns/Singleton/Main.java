package com.PavelFofanov.Generating_Patterns.Singleton;

public class Main {
    public static void main(String[] args) {
        Singleton singleton = Singleton.getInstance("Test 1");
        Singleton singleton2 = Singleton.getInstance("Test 2");
        System.out.println(singleton);
        System.out.println(singleton2);

    }
}
