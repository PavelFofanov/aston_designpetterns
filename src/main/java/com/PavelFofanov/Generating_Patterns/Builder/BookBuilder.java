package com.PavelFofanov.Generating_Patterns.Builder;


public class BookBuilder implements Builder {
    Book book = new Book();
    @Override
    public Builder setBookTitle(String title) {
        book.setTitle(title);
        return this;
    }

    @Override
    public Builder setAuthorOfBook(String author) {
        book.setAuthor(author);
        return this;
    }

    @Override
    public Builder setCost(int cost) {
        book.setCost(cost);
        return this;
    }

    @Override
    public Book build() {
        return book;
    }

}
