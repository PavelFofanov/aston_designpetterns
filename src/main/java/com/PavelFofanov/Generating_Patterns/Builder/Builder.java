package com.PavelFofanov.Generating_Patterns.Builder;

public interface Builder {
    Builder setBookTitle(String title);

    Builder setAuthorOfBook(String author);

    Builder setCost(int cost);

    Book build();
}
