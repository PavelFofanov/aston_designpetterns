package com.PavelFofanov.Generating_Patterns.Builder;

public class Main {
    public static void main(String[] args) {
        Book book = new BookBuilder()
                .setBookTitle("Java")
                .setAuthorOfBook("Sidikov")
                .setCost(100)
                .build();
        System.out.println(book);
    }
}
