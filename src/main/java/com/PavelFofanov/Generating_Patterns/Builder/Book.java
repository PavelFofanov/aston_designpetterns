package com.PavelFofanov.Generating_Patterns.Builder;

import lombok.Builder;

public class Book {
    private String title;
    private String Author;

    private int cost;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", Author='" + Author + '\'' +
                ", cost=" + cost +
                '}';
    }
}
