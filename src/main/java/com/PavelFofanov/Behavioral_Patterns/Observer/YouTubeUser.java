package com.PavelFofanov.Behavioral_Patterns.Observer;

public class YouTubeUser implements Subscriber {
    @Override
    public void showNotification(String text) {
        System.out.println("вышел новый видос: " + text);
    }
}
