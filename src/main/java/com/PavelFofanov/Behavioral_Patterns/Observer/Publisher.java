package com.PavelFofanov.Behavioral_Patterns.Observer;

public interface Publisher {
    public void addSubscriber(Subscriber subscriber);

    public void removeSubscriber(Subscriber subscriber);

    public void notifySubscriber(String text);
}
