package com.PavelFofanov.Behavioral_Patterns.Observer;

public class Main {
    public static void main(String[] args) {
        YouTubeChanel youTubeChanel = new YouTubeChanel();
        Subscriber youTubeUser = new YouTubeUser();
        Subscriber youTubeUser1 = new YouTubeUser();
        Subscriber youTubeUser2 = new YouTubeUser();

        youTubeChanel.uploadNewVideo("Паттерн наблюдатель!!!");

        youTubeChanel.addSubscriber(youTubeUser);
        youTubeChanel.addSubscriber(youTubeUser2);
        youTubeChanel.addSubscriber(youTubeUser1);
        youTubeChanel.uploadNewVideo("2 Паттерн наблюдатель!!!");

        youTubeChanel.removeSubscriber(youTubeUser1);

        youTubeChanel.uploadNewVideo("3 Паттерн наблюдатель!!!");

    }
}
