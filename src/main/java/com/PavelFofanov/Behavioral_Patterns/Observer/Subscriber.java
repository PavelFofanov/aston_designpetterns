package com.PavelFofanov.Behavioral_Patterns.Observer;

public interface Subscriber {
    public void showNotification(String text);
}
