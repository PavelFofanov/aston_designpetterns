package com.PavelFofanov.Structural_Patterns.Adapter;

public class Main {
    public static void main(String[] args) {
        Car car = new AdapterForPlane(new PlaneG6());
        car.drive();
    }
}
