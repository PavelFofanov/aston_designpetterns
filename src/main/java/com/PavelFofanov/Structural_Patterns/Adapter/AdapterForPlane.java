package com.PavelFofanov.Structural_Patterns.Adapter;

public class AdapterForPlane implements Car{
    private Plane plane;
    public AdapterForPlane (Plane plane){
        this.plane = plane;
    }

    @Override
    public void drive() {
        plane.fly();
    }
}
