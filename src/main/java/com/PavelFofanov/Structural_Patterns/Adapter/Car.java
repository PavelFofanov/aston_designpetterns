package com.PavelFofanov.Structural_Patterns.Adapter;

public interface Car {
    void drive();
}
