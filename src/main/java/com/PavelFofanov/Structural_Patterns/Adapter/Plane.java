package com.PavelFofanov.Structural_Patterns.Adapter;

public interface Plane {
    void fly();
}
