package com.PavelFofanov.Structural_Patterns.Decorator;

public class SportTransport implements Transport {
    Transport transport;

    public SportTransport() {
    }

    public SportTransport(Transport transport) {
        this.transport = transport;
    }

    @Override
    public void type() {
        if (transport != null) transport.type();
        System.out.println("Очень быстрая машина");
    }
}
