package com.PavelFofanov.Structural_Patterns.Decorator;

public class Main {
    public static void main(String[] args) {
        Transport transport = new SportTransport(new BaseCar());
        transport.type();
    }

}
