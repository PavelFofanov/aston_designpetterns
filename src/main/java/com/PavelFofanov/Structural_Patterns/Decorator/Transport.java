package com.PavelFofanov.Structural_Patterns.Decorator;

public interface Transport {
    public void type();
}
