package com.PavelFofanov.Structural_Patterns.Decorator;

public class BaseCar implements Transport{
    Transport transport;

    public BaseCar() {
    }

    public BaseCar(Transport transport) {
        this.transport = transport;
    }

    @Override
    public void type() {
        if (transport != null) transport.type();
        System.out.println("Просто едет");
    }
}
