package com.PavelFofanov.Structural_Patterns.Decorator;

public class TransportDecorator implements Transport{
    Transport transport;

    public TransportDecorator(Transport transport) {
        this.transport = transport;
    }

    @Override
    public void type() {

    }
}
