package com.PavelFofanov.Structural_Patterns.Facade;

public class Facade {
    private KeyCar key;
    private EngineCar engine;

    public Facade() {
        key = new KeyCar();
        engine = new EngineCar();
    }
    public void startCar(){
        key.start();
        engine.start();
    }

    public void stopCar(){
        engine.stop();
        key.stop();
    }
}
