package com.PavelFofanov.Structural_Patterns.Facade;

public class Main {
    public static void main(String[] args) {
        Facade facade = new Facade();

        facade.startCar();
        System.out.println("\n ====================== \n");
        facade.stopCar();
    }
}
