package com.PavelFofanov.Structural_Patterns.Facade;

public class EngineCar implements Car {
    @Override
    public void start() {
        System.out.println("Запустить двигатель");
    }

    @Override
    public void stop() {
        System.out.println("Остановить двигатель");
    }
}
