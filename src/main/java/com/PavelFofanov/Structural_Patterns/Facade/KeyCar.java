package com.PavelFofanov.Structural_Patterns.Facade;

public class KeyCar implements Car {
    @Override
    public void start() {
        System.out.println("Вставить ключи");
    }

    @Override
    public void stop() {
        System.out.println("Вытащить ключи");
    }
}
