package com.PavelFofanov.Structural_Patterns.Facade;

public interface Car {
    void start();
    void stop();
}
