package com.PavelFofanov.Structural_Patterns.Proxy;

import java.util.HashMap;
import java.util.Map;

public class SiteProxy implements Site {

    private Site site;
    private final Map<Integer, String> cash;

    public SiteProxy(Site site) {
        this.site = site;
        cash = new HashMap<>();
    }

    @Override
    public String getPage(int num) {
        String page;
        if (cash.containsKey(num)){
            page = cash.get(num);
            page = "Proxy страница: " + page;
        }else {
            page = site.getPage(num);
            cash.put(num, page);
        }
        return page;
    }
}
