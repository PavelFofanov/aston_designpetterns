package com.PavelFofanov.Structural_Patterns.Proxy;

public class Main {
    public static void main(String[] args) {
        Site site = new SiteProxy(new SiteImpl());
        System.out.println(site.getPage(1));
        System.out.println(site.getPage(2));
        System.out.println(site.getPage(3));
        System.out.println(site.getPage(4));

        System.out.println(site.getPage(2));
        System.out.println(site.getPage(4));
    }
}
