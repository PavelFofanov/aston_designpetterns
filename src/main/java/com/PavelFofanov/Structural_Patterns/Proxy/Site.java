package com.PavelFofanov.Structural_Patterns.Proxy;

public interface Site {
    String getPage(int num);
}
