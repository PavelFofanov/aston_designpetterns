package com.PavelFofanov.Structural_Patterns.Proxy;

public class SiteImpl implements Site {
    @Override
    public String getPage(int num) {
        return "Это страница с сервера " + num;
    }
}
